import mongoose from "mongoose";

export type SchoolDocument = mongoose.Document & {
    name: string;
    address: string;
};

const schoolSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true
    },
    address: {
        type: String,
        unique: true
    },
});

export const School = mongoose.model<SchoolDocument>("School", schoolSchema, "schools");
