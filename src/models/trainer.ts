import mongoose, { Schema } from "mongoose";
import { BranchDocument } from "./branch";

export const levels: string[] = [
    "DAN",
    "QUE",
];

export type TrainerDocument = mongoose.Document & {
    name: string;
    surname: string;
    fathersName: string;
    address: string;
    level: number;
    levelType: string;
    birthDate: Date;
    branch: BranchDocument;
    photo: string;
    title: string;
    about: string;
    isMaster: boolean;

    FullName(): string;
};

const trainerSchema = new mongoose.Schema({
    photo: String,
    email: String,
    phone: String,
    title: String,
    about: String,
    passwordHash: String,
    authToken: String,
    name: String,
    surname: String,
    fathersName: String,
    birthDate: Date,
    level: Number,
    levelType: String,
    branch: {
        type: Schema.Types.ObjectId,
        ref: "Branch",
    },
    isMaster: Boolean,
});

trainerSchema.methods.FullName = function() {
    return `${this.surname} ${this.name} ${this.fathersName}`;
};

export const Trainer = mongoose.model<TrainerDocument>("Trainer", trainerSchema, "trainers");
