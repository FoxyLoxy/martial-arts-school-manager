import mongoose, { Schema } from "mongoose";
import { TraineeDocument } from "./trainee";
import { BranchDocument } from "./branch";

export type SeminarDocument = mongoose.Document & {
    date: Date;
    branch: BranchDocument;
    participants: TraineeDocument[];
    description: string;
    protocolUrl: string;
    protocolName: string;
};

const seminarSchema = new mongoose.Schema({
    date: Date,
    branch: {
        type: Schema.Types.ObjectId,
        ref: "Branch",
    },
    participants: [
        {
            type: Schema.Types.ObjectId,
            ref: "Trainee",
        }
    ],
    title: String,
    description: String,
    protocolUrl: String,
    protocolName: String,
});

export const Seminar = mongoose.model<SeminarDocument>("Seminar", seminarSchema, "seminars");
