import mongoose, { Schema } from "mongoose";
import { ExamDocument } from "./exam";

export type TraineeDocument = mongoose.Document & {
    name: string;
    surname: string;
    fathersName: string;
    address: string;
    level: number;
    levelType: string;
    birthDate: Date;
    trainer: TraineeDocument;
    photo: string;
    email: string;
    phone: string;
    exams: {
        exam: ExamDocument;
        notices: string;
        conclusion: string;
        passed: boolean;
    }[];

    FullName(): string;
    PassedExam(examId: string): boolean;
    GetExam(examId: string): ExamDocument;
};

const traineeSchema = new mongoose.Schema({
    name: String,
    surname: String,
    fathersName: String,
    email: String,
    phone: String,
    address: String,
    birthDate: Date,
    level: Number,
    levelType: String,
    trainer: {
        type: Schema.Types.ObjectId,
        ref: "Trainer",
    },
    photo: String,
    exams: [
        {
            exam: {
                type: Schema.Types.ObjectId,
                ref: "Exam",
            },
            notices: String,
            conclusion: String,
            passed: Boolean,
        }
    ],
});

traineeSchema.methods.FullName = function() {
    return `${this.surname} ${this.name} ${this.fathersName}`;
};

traineeSchema.methods.PassedExam = function(examId: string) {
    for (let examEntry of this.exams) {
        if (examEntry.exam._id.toString() === examId && examEntry.passed) {
            return true;
        }
    }
    return false;
};

traineeSchema.methods.GetExam = function(examId: string) {
    for (let examEntry of this.exams) {
        if (examEntry.exam._id.toString() === examId) {
            return examEntry;
        }
    }

    return undefined;
};

export const Trainee = mongoose.model<TraineeDocument>("Trainee", traineeSchema, "trainees");
