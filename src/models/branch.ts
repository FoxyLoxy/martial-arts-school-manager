import mongoose, {Schema} from "mongoose";
import { SchoolDocument } from "./school";

export type BranchDocument = mongoose.Document & {
    name: string;
    school: SchoolDocument;
};

const branchSchema = new mongoose.Schema({
    name: String,
    school: {
        type: Schema.Types.ObjectId,
        ref: "School",
    },
});

export const Branch = mongoose.model<BranchDocument>("Branch", branchSchema, "branches");
