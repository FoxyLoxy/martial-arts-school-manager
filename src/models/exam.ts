import mongoose, { Schema } from "mongoose";
import { TraineeDocument } from "./trainee";
import { BranchDocument } from "./branch";

export type ExamDocument = mongoose.Document & {
    date: Date;
    branch: BranchDocument;
    participants: TraineeDocument[];
};

const examSchema = new mongoose.Schema({
    date: Date,
    branch: {
        type: Schema.Types.ObjectId,
        ref: "Branch",
    },
    participants: [
        {
            type: Schema.Types.ObjectId,
            ref: "Trainee",
        }
    ],
});

export const Exam = mongoose.model<ExamDocument>("Exam", examSchema, "exams");
