import mongoose, {Schema} from "mongoose";
import { SchoolDocument } from "./school";
import { TrainerDocument } from "./trainer";

export type BlogPostDocument = mongoose.Document & {
    author: TrainerDocument;
    title: string;
    quillDeltaJson: SchoolDocument;
    date: Date;
};

const blogPostSchema = new mongoose.Schema({
    title: String,
    quillDeltaJson: String,
    author: {
        type: Schema.Types.ObjectId,
        ref: "Trainer",
    },
    date: Date,
});

export const BlogPost = mongoose.model<BlogPostDocument>("BlogPost", blogPostSchema, "blogPosts");
