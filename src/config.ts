interface Config {
    appPort: number;
    cookieSecret: string;
    dbUrl: string;
}

const config = {
    appPort: process.env.PORT || 3000,
    cookieSecret: process.env.COOKIE_SECRET || "fallbackSecret",
    dbUrl: process.env.DB_URL || "mongodb://127.0.0.1/schoolManager",
} as Config;

export default config;
