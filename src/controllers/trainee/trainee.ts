import { Request, Response } from "express";
import { Exam, ExamDocument } from "../../models/exam";
import { Trainee, TraineeDocument } from "../../models/trainee";
import { Branch, BranchDocument } from "../../models/branch";
import { Trainer, TrainerDocument } from "../../models/trainer";
import { Seminar, SeminarDocument } from "../../models/seminar";

export function Index(req: Request, res: Response) {
    res.render("trainee/index");
}

export function ShowExams(req: Request, res: Response) {
    Exam.find().populate("branch").sort("date").then((exams: ExamDocument[]) => {
        res.render("trainee/exams", {
            exams
        });
    });
}

export function ShowExam(req: Request, res: Response) {
    Exam.findById(
        req.params.id
    )
        .populate('branch')
        .populate('participants')
        .then((exam: ExamDocument | null) => {
            if (exam === null) {
                res.redirect("/trainee/exam");
            } else {
                res.render("trainee/exam", {
                    exam,
                });
            }
        });
}

export function ShowExamReport(req: Request, res: Response) {
    Exam.findOne(
        {
            _id: req.params.examId
        }
    )
        .populate('branch')
        .then((exam: ExamDocument | null) => {
            if (exam === null) {
                res.redirect('/trainee/exam');
            } else {
                Trainee.findOne({
                    _id: req.params.traineeId
                }).then((trainee: TraineeDocument | null) => {
                    if (trainee === null) {
                        res.redirect('/trainee/exam');
                    } else {
                        res.render("trainee/examReport", {
                            exam,
                            trainee,
                        });
                    }
                });
            }
        });
}

export function ShowSeminars(req: Request, res: Response) {
    Seminar.find().populate('branch').then((seminars: SeminarDocument[]) => {
        res.render("trainee/seminars", {
            seminars,
        });
    });
}

export function ShowSeminar(req: Request, res: Response) {
    Seminar.findById(req.params.id).populate('branch').populate('participants').then((seminar: SeminarDocument | null) => {
        if (seminar === null) {
            res.redirect("/trainee/seminar");
        } else {
            res.render("trainee/seminar", {
                seminar,
            });
        }
    });
}

export function ShowBranches(req: Request, res: Response) {
    Branch.find().populate('school').then((branches: BranchDocument[]) => {
        res.render("trainee/branches", {
            branches,
        });
    });
}

export function ShowBranchTrainees(req: Request, res: Response) {
    Trainer.find({
        branch: req.params.id,
    })
        .then((trainers: TrainerDocument[]) => {
            return trainers;
        })
        .then((trainers: TrainerDocument[]) => {
            return Trainee.find({
                trainer: {
                    $in: trainers,
                },
            }).populate('trainer');
        })
        .then((trainees: TraineeDocument[]) => {
            Branch.findById(req.params.id).then((branch: BranchDocument | null) => {
                if (branch === null) {
                    res.redirect("/trainee/branch");
                } else {
                    res.render("trainee/branchTrainees", {
                        trainees,
                        branch,
                    });
                }
            });
        });
}

export function ShowTrainee(req: Request, res: Response) {
    Trainee.findById(req.params.id)
        .populate('trainer')
        .populate('exams.exam')
        .then((trainee: TraineeDocument | null) => {
            if (trainee === null) {
                res.redirect("/trainee/branch");
            } else {
                res.render("trainee/traineeInfo", {
                    trainee,
                });
            }
        });
}

export function ShowBranchTrainers(req: Request, res: Response) {
    Trainer.find({
        branch: req.params.id,
    }).populate('branch')
        .then((trainers: TrainerDocument[]) => {
            Branch.findById(req.params.id).then((branch: BranchDocument | null) => {
                if (branch === null) {
                    res.redirect("/trainee/branch");
                } else {
                    res.render("trainee/branchTrainers", {
                        trainers,
                        branch,
                    });
                }
            });
        });
}

export function ShowTrainer(req: Request, res: Response) {
    Trainer.findById(req.params.id).populate('branch').then((trainer: TrainerDocument | null) => {
        if (trainer === null) {
            res.redirect("/trainee/branch");
        } else {
            Trainee.find({
                trainer: trainer._id
            }).then((trainees: TraineeDocument[]) => {
                res.render("trainee/trainerInfo", {
                    trainer,
                    trainees,
                });
            });
        }
    });
}
