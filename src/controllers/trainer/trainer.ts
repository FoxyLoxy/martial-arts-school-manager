import bcrypt from "bcrypt";

import { Request, Response } from "express";
import { Trainer, TrainerDocument } from "../../models/trainer";
import { validationResult } from "express-validator";
import { Flash } from "../../helpers/flash";
import { Branch, BranchDocument } from "../../models/branch";

export function Home(req: Request, res: Response) {
    res.render("trainer/index");
}

export function Index(req: Request, res: Response) {
    Trainer.find().populate('branch').then((trainers: TrainerDocument[]) => {
        res.render("trainer/trainer", {
            trainers,
            flash: req.flash('flash'),
        });
    });
}

export function Create(req: Request, res: Response) {
    Branch.find().then((branches: BranchDocument[]) => {
        res.render("trainer/trainerForm",
            {
                branches,
                trainer: {},
                valErrors: {},
            });
    }).catch(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "warning",
            message: "Unable to get branch list",
        } as Flash);
        res.redirect("/trainer/trainer");
    });
}

export function Show(req: Request, res: Response) {
    Trainer.findOne(
        {
            _id: req.params.id
        }
    ).then((trainer: TrainerDocument | null) => {
        if (trainer === null) {
            res.redirect("/trainer/trainer/edit");
        } else {
            Branch.find().then((branches: BranchDocument[]) => {
                res.render("trainer/trainerForm", {
                    trainer,
                    branches
                });
            }).catch(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "warning",
                    message: "Duplicate entry",
                } as Flash);
                res.redirect("/trainer/trainer");
            });
        }
    }).catch(() => {
        res.redirect("/trainer/trainer");
    });
}

export function Store(req: Request, res: Response) {
    const valErr = validationResult(req);
    if (!valErr.isEmpty()) {
        Branch.find().then((branches: BranchDocument[]) => {
            req.body._id = req.body.id;
            res.render("trainer/trainerForm", {
                trainer: req.body,
                branches: branches,
                valErrors: valErr.array(),
            });
        });
    } else {
        if (req.body.id) {
            let docToUpdate: any = {
                name: req.body.name,
                title: req.body.title,
                about: req.body.about,
                surname: req.body.surname,
                fathersName: req.body.fathersName,
                email: req.body.email,
                phone: req.body.phone,
                birthDate: req.body.birthDate,
                level: req.body.level,
                levelType: req.body.levelType,
                branch: req.body.branch_id,
                isMaster: req.body.isMaster === "on",
            };
            if (req.body.password != "") {
                docToUpdate.passwordHash = bcrypt.hashSync(req.body.password, 10);
            }
            Trainer.updateOne(
                {
                    _id: req.body.id,
                }, docToUpdate).then(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "success",
                    message: "Updated the entry",
                } as Flash);
            }).catch((reason) => {
                // @ts-ignore
                req.flash('flash', {
                    type: "warning",
                    message: "Failed updating the entry",
                } as Flash);
            }).finally(() => {
                res.redirect("/trainer/trainer");
            });
        } else {
            console.log(req.body);
            Trainer.findOneAndUpdate(
                {
                    name: req.body.name,
                    surname: req.body.surname,
                    fathersName: req.body.fathersName,
                    title: req.body.title,
                    about: req.body.about,
                    passwordHash: bcrypt.hashSync(req.body.password, 10),
                    email: req.body.email,
                    phone: req.body.phone,
                    birthDate: req.body.birthDate,
                    level: req.body.level,
                    levelType: req.body.levelType,
                    branch: req.body.branch_id,
                    isMaster: req.body.isMaster === "on",
                }, {
                    name: req.body.name,
                    surname: req.body.surname,
                    title: req.body.title,
                    about: req.body.about,
                    fathersName: req.body.fathersName,
                    passwordHash: bcrypt.hashSync(req.body.password, 10),
                    email: req.body.email,
                    phone: req.body.phone,
                    birthDate: req.body.birthDate,
                    level: req.body.level,
                    levelType: req.body.levelType,
                    branch: req.body.branch_id,
                    isMaster: req.body.isMaster === "on",
                }, {
                    upsert: true,
                    new: true
                })
                .then(() => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "success",
                        message: "Added new entry",
                    } as Flash);
                })
                .catch((reason) => {
                    console.log(reason);
                    // @ts-ignore
                    req.flash('flash', {
                        type: "warning",
                        message: "Duplicate entry",
                    } as Flash);
                })
                .finally(() => {
                    res.redirect("/trainer/trainer");
                });
        }
    }
}

export function Delete(req: Request, res: Response) {
    Trainer.deleteOne(
        {
            _id: req.params.id
        }
    ).then(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "success",
            message: "Deleted entry successfully",
        } as Flash);
    }).catch(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "warning",
            message: "Unable to delete entry",
        } as Flash);
    }).finally(() => {
        res.redirect("/trainer/trainer");
    });
}

