import { Request, Response } from "express";
import { School, SchoolDocument } from "../../models/school";
import { validationResult } from "express-validator";
import { Flash } from "../../helpers/flash";

export function Index(req: Request, res: Response) {
    School.find().then((schools: SchoolDocument[]) => {
        res.render("trainer/school", {
            schools,
            flash: req.flash('flash'),
        });
    });
}

export function Create(req: Request, res: Response) {
    res.render("trainer/schoolForm",
        {
            school: {},
            valErrors: {},
        });
}

export function Show(req: Request, res: Response) {
    School.findOne(
        {
            _id: req.params.id
        }
    ).then((school: SchoolDocument | null) => {
        if (school === null) {
            res.redirect("/trainer/school/edit");
        } else {
            res.render("trainer/schoolForm", {
                school
            });
        }
    }).catch(() => {
        res.redirect("/trainer/school");
    });
}

export function Store(req: Request, res: Response) {
    const valErr = validationResult(req);
    if (!valErr.isEmpty()) {
        req.body._id = req.body.id;
        res.render("trainer/schoolForm", {
            school: req.body,
            valErrors: valErr.array(),
        });
    } else {
        if (req.body.id) {
            School.updateOne(
                {
                    _id: req.body.id,
                }, {
                    name: req.body.name,
                    address: req.body.address
                }).then(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "success",
                    message: "Updated the entry",
                } as Flash);
            }).catch(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "warning",
                    message: "Failed updating the entry",
                } as Flash);
            }).finally(() => {
                res.redirect("/trainer/school");
            });
        } else {
            School.findOneAndUpdate(
                {
                    name: req.body.name,
                    address: req.body.address
                }, {
                    name: req.body.name,
                    address: req.body.address
                }, {
                    upsert: true,
                    new: true
                })
                .then(() => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "success",
                        message: "Added new entry",
                    } as Flash);
                })
                .catch(() => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "warning",
                        message: "Duplicate entry",
                    } as Flash);
                })
                .finally(() => {
                    res.redirect("/trainer/school");
                });
        }
    }
}

export function Delete(req: Request, res: Response) {
    School.deleteOne(
        {
            _id: req.params.id
        }
    ).then(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "success",
            message: "Deleted entry successfully",
        } as Flash);
    }).catch(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "warning",
            message: "Unable to delete entry",
        } as Flash);
    }).finally(() => {
        res.redirect("/trainer/school");
    });
}

