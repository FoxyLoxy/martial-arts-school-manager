import { Request, Response } from "express";
import { Branch, BranchDocument } from "../../models/branch";
import { validationResult } from "express-validator";
import { Flash } from "../../helpers/flash";
import { Exam, ExamDocument } from "../../models/exam";
import { Trainee, TraineeDocument } from "../../models/trainee";
import { Seminar, SeminarDocument } from "../../models/seminar";

export function Index(req: Request, res: Response) {
    Seminar.find().populate('branch').then((seminars: SeminarDocument[]) => {
        res.render("trainer/seminar", {
            seminars,
            flash: req.flash('flash'),
        });
    });
}

export function Create(req: Request, res: Response) {
    Branch.find().then((branches: BranchDocument[]) => {
        return Trainee.find().then((trainees: TraineeDocument[]) => {
            return {
                branches,
                trainees
            };
        });
    })
        .then((obj) => {
            res.render("trainer/seminarForm",
                {
                    branches: obj.branches,
                    trainees: obj.trainees,
                    seminar: {},
                    valErrors: {},
                });
        })
        .catch(() => {
            // @ts-ignore
            req.flash('flash', {
                type: "warning",
                message: "Unable to get branch or trainee list",
            } as Flash);
            res.redirect("/trainer/exam");
        });
}

export function Show(req: Request, res: Response) {
    Seminar.findOne(
        {
            _id: req.params.id
        }
    ).then((seminar: SeminarDocument | null) => {
        if (seminar === null) {
            res.redirect("/trainer/exam/edit");
        } else {
            Branch.find().then((branches: BranchDocument[]) => {
                return Trainee.find().then((trainees: TraineeDocument[]) => {
                    res.render("trainer/seminarForm", {
                        branches,
                        trainees,
                        seminar,
                        selectedTrainees: seminar.participants,
                    });
                });
            }).catch(() => {
                res.redirect("/trainer/seminar");
            });
        }
    }).catch(() => {
        res.redirect("/trainer/seminar");
    });
}

export function Store(req: Request, res: Response) {
    const valErr = validationResult(req);
    if (!valErr.isEmpty()) {
        Seminar.find()
            .then((branches: SeminarDocument[]) => {
                return Trainee.find().then((trainees: TraineeDocument[]) => {
                    return {
                        branches,
                        trainees
                    };
                });
            })
            .then((obj) => {
                req.body._id = req.body.id;
                res.render("trainer/seminarForm",
                    {
                        seminar: req.body,
                        branches: obj.branches,
                        trainees: obj.trainees,
                        selectedTrainees: req.body.selectedTrainees,
                        selectedBranch: req.body.branch_id,
                        valErrors: valErr.array(),
                    });
            });
    } else {
        if (req.body.id) {
            Seminar.updateOne(
                {
                    _id: req.body.id,
                }, {
                    title: req.body.title,
                    description: req.body.description,
                    date: req.body.date,
                    branch: req.body.branch_id,
                    participants: req.body.selectedTrainees,
                }).then(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "success",
                    message: "Updated the entry",
                } as Flash);
            }).catch(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "warning",
                    message: "Failed updating the entry",
                } as Flash);
            }).finally(() => {
                res.redirect("/trainer/seminar");
            });
        } else {
            Seminar.findOneAndUpdate(
                {
                    title: req.body.title,
                    description: req.body.description,
                    date: req.body.date,
                    branch: req.body.branch_id,
                    participants: req.body.selectedTrainees,
                }, {
                    title: req.body.title,
                    description: req.body.description,
                    date: req.body.date,
                    branch: req.body.branch_id,
                    participants: req.body.selectedTrainees,
                }, {
                    upsert: true,
                    new: true
                })
                .then(() => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "success",
                        message: "Added new entry",
                    } as Flash);
                })
                .catch((err) => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "warning",
                        message: "Duplicate entry",
                    } as Flash);
                })
                .finally(() => {
                    res.redirect("/trainer/seminar");
                });
        }
    }
}

export function Delete(req: Request, res: Response) {
    Seminar.deleteOne(
        {
            _id: req.params.id
        }
    ).then(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "success",
            message: "Deleted entry successfully",
        } as Flash);
    }).catch(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "warning",
            message: "Unable to delete entry",
        } as Flash);
    }).finally(() => {
        res.redirect("/trainer/seminar");
    });
}

export function StoreProtocol(req: Request, res: Response) {
    Seminar.findById(req.params.id).then((seminar: SeminarDocument | null) => {
        if (seminar === null) {
            res.json({
                err: "no such seminar"
            });
        } else {
            const ts = Date.now().toString(10);
            // @ts-ignore
            const fileName = req.files.file.name;
            // @ts-ignore
            const filePath = `/public/files/${ts}_${req.files.file.name}`;

            seminar.protocolUrl = filePath;
            seminar.protocolName = fileName;
            seminar.save()
                .then(() => {
                    // @ts-ignore
                    req.files.file.mv(`./public/${ts}_${req.files.file.name}`,).then(() => {
                        res.json({
                            fileName,
                            filePath,
                        });
                    });
                })
                .catch(() => {
                    res.json({
                        err: "error saving new file"
                    });
                });
        }
    });
}
