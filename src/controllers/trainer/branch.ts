import { Request, Response } from "express";
import { Branch, BranchDocument } from "../../models/branch";
import { validationResult } from "express-validator";
import { Flash } from "../../helpers/flash";
import { School, SchoolDocument } from "../../models/school";

export function Index(req: Request, res: Response) {
    Branch.find().populate('school').then((branches: BranchDocument[]) => {
        res.render("trainer/branch", {
            branches,
            flash: req.flash('flash'),
        });
    });
}

export function Create(req: Request, res: Response) {
    School.find().then((schools: SchoolDocument[]) => {
        res.render("trainer/branchForm",
            {
                schools,
                branch: {},
                valErrors: {},
            });
    }).catch(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "warning",
            message: "Unable to get school list",
        } as Flash);
        res.redirect("/trainer/branch");
    });
}

export function Show(req: Request, res: Response) {
    Branch.findOne(
        {
            _id: req.params.id
        }
    ).then((branch: BranchDocument | null) => {
        if (branch === null) {
            res.redirect("/trainer/branch/edit");
        } else {
            School.find().then((schools: SchoolDocument[]) => {
                res.render("trainer/branchForm", {
                    branch,
                    schools
                });
            }).catch(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "warning",
                    message: "Duplicate entry",
                } as Flash);
                res.redirect("/trainer/branch");
            });
        }
    }).catch(() => {
        res.redirect("/trainer/branch");
    });
}

export function Store(req: Request, res: Response) {
    const valErr = validationResult(req);
    if (!valErr.isEmpty()) {
        req.body._id = req.body.id;
        res.render("trainer/branchForm", {
            branch: req.body,
            valErrors: valErr.array(),
        });
    } else {
        if (req.body.id) {
            Branch.updateOne(
                {
                    _id: req.body.id,
                }, {
                    name: req.body.name,
                    school: req.body.school_id
                }).then(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "success",
                    message: "Updated the entry",
                } as Flash);
            }).catch(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "warning",
                    message: "Failed updating the entry",
                } as Flash);
            }).finally(() => {
                res.redirect("/trainer/branch");
            });
        } else {
            Branch.findOneAndUpdate(
                {
                    name: req.body.name,
                    school: req.body.school_id
                }, {
                    name: req.body.name,
                    school: req.body.school_id
                }, {
                    upsert: true,
                    new: true
                })
                .then(() => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "success",
                        message: "Added new entry",
                    } as Flash);
                })
                .catch(() => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "warning",
                        message: "Duplicate entry",
                    } as Flash);
                })
                .finally(() => {
                    res.redirect("/trainer/branch");
                });
        }
    }
}

export function Delete(req: Request, res: Response) {
    Branch.deleteOne(
        {
            _id: req.params.id
        }
    ).then(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "success",
            message: "Deleted entry successfully",
        } as Flash);
    }).catch(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "warning",
            message: "Unable to delete entry",
        } as Flash);
    }).finally(() => {
        res.redirect("/trainer/branch");
    });
}

