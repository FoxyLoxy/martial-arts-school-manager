import { Request, Response } from "express";
import { Branch, BranchDocument } from "../../models/branch";
import { validationResult } from "express-validator";
import { Flash } from "../../helpers/flash";
import { Exam, ExamDocument } from "../../models/exam";
import { Trainee, TraineeDocument } from "../../models/trainee";

export function Index(req: Request, res: Response) {
    Exam.find().populate('branch').then((exams: ExamDocument[]) => {
        res.render("trainer/exam", {
            exams,
            flash: req.flash('flash'),
        });
    });
}

export function Create(req: Request, res: Response) {
    Branch.find().then((branches: BranchDocument[]) => {
        return Trainee.find().then((trainees: TraineeDocument[]) => {
            return {
                branches,
                trainees
            };
        });
    })
        .then((obj) => {
            res.render("trainer/examForm",
                {
                    branches: obj.branches,
                    trainees: obj.trainees,
                    exam: {},
                    valErrors: {},
                });
        })
        .catch(() => {
            // @ts-ignore
            req.flash('flash', {
                type: "warning",
                message: "Unable to get branch or trainee list",
            } as Flash);
            res.redirect("/trainer/exam");
        });
}

export function Show(req: Request, res: Response) {
    Exam.findOne(
        {
            _id: req.params.id
        }
    ).then((exam: ExamDocument | null) => {
        if (exam === null) {
            res.redirect("/trainer/exam/edit");
        } else {
            Branch.find().then((branches: BranchDocument[]) => {
                return Trainee.find().then((trainees: TraineeDocument[]) => {
                    res.render("trainer/examForm", {
                        branches,
                        trainees,
                        exam,
                        selectedTrainees: exam.participants,
                    });
                });
            }).catch(() => {
                res.redirect("/trainer/exam");
            });
        }
    }).catch(() => {
        res.redirect("/trainer/exam");
    });
}

export function Store(req: Request, res: Response) {
    const valErr = validationResult(req);
    if (!valErr.isEmpty()) {
        Branch.find()
            .then((branches: BranchDocument[]) => {
                return Trainee.find().then((trainees: TraineeDocument[]) => {
                    return {
                        branches,
                        trainees
                    };
                });
            })
            .then((obj) => {
                req.body._id = req.body.id;
                res.render("trainer/examForm",
                    {
                        exam: req.body,
                        branches: obj.branches,
                        trainees: obj.trainees,
                        selectedTrainees: req.body.selectedTrainees,
                        selectedBranch: req.body.branch_id,
                        valErrors: valErr.array(),
                    });
            });
    } else {
        if (req.body.id) {
            Exam.updateOne(
                {
                    _id: req.body.id,
                }, {
                    date: req.body.date,
                    branch: req.body.branch_id,
                    participants: req.body.selectedTrainees,
                }).then(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "success",
                    message: "Updated the entry",
                } as Flash);
            }).catch(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "warning",
                    message: "Failed updating the entry",
                } as Flash);
            }).finally(() => {
                res.redirect("/trainer/exam");
            });
        } else {
            Exam.findOneAndUpdate(
                {
                    date: req.body.date,
                    branch: req.body.branch_id,
                    participants: req.body.selectedTrainees,
                }, {
                    date: req.body.date,
                    branch: req.body.branch_id,
                    participants: req.body.selectedTrainees,
                }, {
                    upsert: true,
                    new: true
                })
                .then(() => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "success",
                        message: "Added new entry",
                    } as Flash);
                })
                .catch((err) => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "warning",
                        message: "Duplicate entry",
                    } as Flash);
                })
                .finally(() => {
                    res.redirect("/trainer/exam");
                });
        }
    }
}

export function Delete(req: Request, res: Response) {
    Exam.deleteOne(
        {
            _id: req.params.id
        }
    ).then(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "success",
            message: "Deleted entry successfully",
        } as Flash);
    }).catch(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "warning",
            message: "Unable to delete entry",
        } as Flash);
    }).finally(() => {
        res.redirect("/trainer/exam");
    });
}

export function ShowAllTraineeConclusion(req: Request, res: Response) {
    Exam.findOne(
        {
            _id: req.params.id
        }
    )
        .populate('participants')
        .populate('branch')
        .then((exam: ExamDocument | null) => {
            if (exam === null) {
                res.redirect('/trainer/exam');
            } else {
                res.render("trainer/examTrainees", {
                    exam,
                    flash: req.flash('flash'),
                });
            }
        });
}

export function ShowTraineeConclusion(req: Request, res: Response) {
    Exam.findOne(
        {
            _id: req.params.examId
        }
    )
        .populate('branch')
        .then((exam: ExamDocument | null) => {
            if (exam === null) {
                res.redirect('/trainer/exam');
            } else {
                Trainee.findOne({
                    _id: req.params.traineeId
                }).then((trainee: TraineeDocument | null) => {
                    if (trainee === null) {
                        res.redirect('/trainer/exam');
                    } else {
                        res.render("trainer/examTraineeConclusion", {
                            exam,
                            trainee,
                        });
                    }
                });
            }
        });
}

export function StoreTraineeConclusion(req: Request, res: Response) {
    Trainee.findOne({
        _id: req.params.traineeId,
    }).then((trainee: TraineeDocument | null) => {
        if (trainee === null) {
            res.redirect('/trainer/exam');
        } else {
            let presists = false;
            for (let examEntry of trainee.exams) {
                if (examEntry.exam._id.toString() === req.params.examId) {
                    examEntry.notices = req.body.notices;
                    examEntry.conclusion = req.body.conclusion;
                    examEntry.passed = req.body.passed === 'on';
                    presists = true;
                    trainee.save().then(() => {
                        // @ts-ignore
                        req.flash('flash', {
                            type: "success",
                            message: "Modified entry successfully",
                        } as Flash);
                    }).catch((err) => {
                        // @ts-ignore
                        req.flash('flash', {
                            type: "warning",
                            message: "Unable to modify entry",
                        } as Flash);
                    });
                }
            }

            if (!presists) {
                Exam.countDocuments({
                    _id: req.params.examId
                }).then((count: number) => {
                    if (count > 0) {
                        Trainee.updateOne({
                            _id: req.params.traineeId,
                        }, {
                            $push: {
                                exams: {
                                    exam: req.params.examId,
                                    passed: req.body.passed === 'on',
                                    notices: req.body.notices,
                                    conclusion: req.body.conclusion,
                                }
                            }
                        }).then(() => {
                            // @ts-ignore
                            req.flash('flash', {
                                type: "success",
                                message: "Modified entry successfully",
                            } as Flash);
                        });
                    }
                });
            }

            res.redirect(`/trainer/exam/show/${req.params.examId}`);
        }
    });
}
