import bcrypt from "bcrypt";

import { Request, Response } from "express";
import { Trainee, TraineeDocument } from "../../models/trainee";
import { validationResult } from "express-validator";
import { Flash } from "../../helpers/flash";
import { Branch, BranchDocument } from "../../models/branch";
import { Trainer, TrainerDocument } from "../../models/trainer";

export function Index(req: Request, res: Response) {
    Trainee.find().populate({
        path: 'trainer',
        populate: {
            path: "branch"
        }
    }).then((trainees: TraineeDocument[]) => {
        console.log(trainees);
        res.render("trainer/trainee", {
            trainees,
            flash: req.flash('flash'),
        });
    });
}

export function Create(req: Request, res: Response) {
    Trainer.find().then((trainers: TrainerDocument[]) => {
        res.render("trainer/traineeForm",
            {
                trainers,
                trainee: {},
                valErrors: {},
            });
    }).catch(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "warning",
            message: "Unable to get branch list",
        } as Flash);
        res.redirect("/trainer/trainee");
    });
}

export function Show(req: Request, res: Response) {
    Trainee.findOne(
        {
            _id: req.params.id
        }
    ).then((trainee: TraineeDocument | null) => {
        if (trainee === null) {
            res.redirect("/trainer/trainee/edit");
        } else {
            Trainer.find().then((trainers: TrainerDocument[]) => {
                res.render("trainer/traineeForm", {
                    trainee,
                    trainers
                });
            }).catch(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "warning",
                    message: "Duplicate entry",
                } as Flash);
                res.redirect("/trainer/trainee");
            });
        }
    }).catch(() => {
        res.redirect("/trainer/trainee");
    });
}

export function Store(req: Request, res: Response) {
    const valErr = validationResult(req);
    if (!valErr.isEmpty()) {
        Branch.find().then((branches: BranchDocument[]) => {
            req.body._id = req.body.id;
            res.render("trainer/traineeForm", {
                trainee: req.body,
                branches: branches,
                valErrors: valErr.array(),
            });
        });
    } else {
        if (req.body.id) {
            let docToUpdate: any = {
                name: req.body.name,
                surname: req.body.surname,
                fathersName: req.body.fathersName,
                email: req.body.email,
                phone: req.body.phone,
                birthDate: req.body.birthDate,
                level: req.body.level,
                levelType: req.body.levelType,
                trainer: req.body.trainer_id,
            };
            Trainee.updateOne(
                {
                    _id: req.body.id,
                }, docToUpdate).then(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "success",
                    message: "Updated the entry",
                } as Flash);
            }).catch((reason) => {
                // @ts-ignore
                req.flash('flash', {
                    type: "warning",
                    message: "Failed updating the entry",
                } as Flash);
            }).finally(() => {
                res.redirect("/trainer/trainee");
            });
        } else {
            console.log(req.body);
            Trainee.findOneAndUpdate(
                {
                    name: req.body.name,
                    surname: req.body.surname,
                    fathersName: req.body.fathersName,
                    email: req.body.email,
                    phone: req.body.phone,
                    birthDate: req.body.birthDate,
                    level: req.body.level,
                    levelType: req.body.levelType,
                    trainer: req.body.trainer_id,
                }, {
                    name: req.body.name,
                    surname: req.body.surname,
                    fathersName: req.body.fathersName,
                    email: req.body.email,
                    phone: req.body.phone,
                    birthDate: req.body.birthDate,
                    level: req.body.level,
                    levelType: req.body.levelType,
                    trainer: req.body.trainer_id,
                }, {
                    upsert: true,
                    new: true
                })
                .then(() => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "success",
                        message: "Added new entry",
                    } as Flash);
                })
                .catch((reason) => {
                    console.log(reason);
                    // @ts-ignore
                    req.flash('flash', {
                        type: "warning",
                        message: "Duplicate entry",
                    } as Flash);
                })
                .finally(() => {
                    res.redirect("/trainer/trainee");
                });
        }
    }
}

export function Delete(req: Request, res: Response) {
    Trainee.deleteOne(
        {
            _id: req.params.id
        }
    ).then(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "success",
            message: "Deleted entry successfully",
        } as Flash);
    }).catch(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "warning",
            message: "Unable to delete entry",
        } as Flash);
    }).finally(() => {
        res.redirect("/trainee/trainee");
    });
}

