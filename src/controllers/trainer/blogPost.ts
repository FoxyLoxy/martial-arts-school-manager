import { Request, Response } from "express";
import { Branch, BranchDocument } from "../../models/branch";
import { validationResult } from "express-validator";
import { Flash } from "../../helpers/flash";
import { School, SchoolDocument } from "../../models/school";
import { BlogPost, BlogPostDocument } from "../../models/blogPost";

export function Index(req: Request, res: Response) {
    BlogPost.find().then((posts: BlogPostDocument[]) => {
        res.render("trainer/blogPost", {
            posts,
            flash: req.flash('flash'),
        });
    });
}

export function Create(req: Request, res: Response) {
    res.render("trainer/blogPostForm",
        {
            post: {},
            valErrors: {},
        });
}


export function Show(req: Request, res: Response) {
    BlogPost.findOne(
        {
            _id: req.params.id
        }
    ).then((post: BlogPostDocument | null) => {
        if (post === null) {
            res.redirect("/trainer/post/edit");
        } else {
            res.render("trainer/blogPostForm", {
                post,
            });
        }
    }).catch(() => {
        res.redirect("/trainer/blogPost");
    });
}

export function Store(req: Request, res: Response) {
    console.log(req.body.date);
    const valErr = validationResult(req);
    if (!valErr.isEmpty()) {
        req.body._id = req.body.id;
        res.render("trainer/blogPostForm", {
            post: req.body,
            valErrors: valErr.array(),
        });
    } else {
        if (req.body.id) {
            BlogPost.updateOne(
                {
                    _id: req.body.id,
                }, {
                    title: req.body.title,
                    date: req.body.date,
                    quillDeltaJson: req.body.quill,
                }).then(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "success",
                    message: "Updated the entry",
                } as Flash);
            }).catch(() => {
                // @ts-ignore
                req.flash('flash', {
                    type: "warning",
                    message: "Failed updating the entry",
                } as Flash);
            }).finally(() => {
                res.redirect("/trainer/blogPost");
            });
        } else {
            BlogPost.findOneAndUpdate(
                {
                    title: req.body.title,
                    date: req.body.date,
                    quillDeltaJson: req.body.quill,
                }, {
                    title: req.body.title,
                    date: req.body.date,
                    quillDeltaJson: req.body.quill,
                }, {
                    upsert: true,
                    new: true
                })
                .then(() => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "success",
                        message: "Added new entry",
                    } as Flash);
                })
                .catch(() => {
                    // @ts-ignore
                    req.flash('flash', {
                        type: "warning",
                        message: "Duplicate entry",
                    } as Flash);
                })
                .finally(() => {
                    res.redirect("/trainer/blogPost");
                });
        }
    }
}

export function Delete(req: Request, res: Response) {
    BlogPost.deleteOne(
        {
            _id: req.params.id
        }
    ).then(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "success",
            message: "Deleted entry successfully",
        } as Flash);
    }).catch(() => {
        // @ts-ignore
        req.flash('flash', {
            type: "warning",
            message: "Unable to delete entry",
        } as Flash);
    }).finally(() => {
        res.redirect("/trainer/branch");
    });
}

