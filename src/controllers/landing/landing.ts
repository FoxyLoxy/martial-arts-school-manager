import { Request, Response } from "express";
import { BlogPost, BlogPostDocument } from "../../models/blogPost";

export function Index(req: Request, res: Response) {
    BlogPost.find().then((posts: BlogPostDocument[]) => {
        res.render("landing/index", {
            posts,
        });
    });
}

export function ShowPost(req: Request, res: Response) {
    BlogPost.findById(req.params.id).then((post: BlogPostDocument | null) => {
        if (post === null) {
            res.redirect('/');
        }  else {
            res.render("landing/post", {
                post,
            });
        }
    });
}
