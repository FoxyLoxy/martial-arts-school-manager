import app from './app';
import config from "./config";
import { connectMongo } from "./db/mongo";

connectMongo().then(() => {
    app.listen(config.appPort).on("error", (err) => {
        console.log('pizda server', err);
        process.exit(1);
    });
}).catch(() => {
    console.log('pizda mongo');
    process.exit(1);
});
