import config from "../config";
import mongoose from "mongoose";

export async function connectMongo() {
    return mongoose.connect(config.dbUrl, {
        useNewUrlParser: true
    });
}
