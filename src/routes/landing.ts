import { Router } from 'express';
import { Index, ShowPost } from "../controllers/landing/landing";

const landingRoutes = Router();

landingRoutes.get("", Index);
landingRoutes.get("/post/:id", ShowPost);

export default landingRoutes;
