import { Router } from 'express';

import * as TraineeController from "../controllers/trainee/trainee";

const traineeRoutes = Router();

traineeRoutes.get("", TraineeController.Index);
traineeRoutes.get("/exam", TraineeController.ShowExams);
traineeRoutes.get("/exam/:id", TraineeController.ShowExam);
traineeRoutes.get("/exam/:examId/:traineeId", TraineeController.ShowExamReport);

traineeRoutes.get("/seminar", TraineeController.ShowSeminars);
traineeRoutes.get("/seminar/:id", TraineeController.ShowSeminar);

traineeRoutes.get("/branch", TraineeController.ShowBranches);
traineeRoutes.get("/branch/trainee/:id", TraineeController.ShowBranchTrainees);
traineeRoutes.get("/branch/trainer/:id", TraineeController.ShowBranchTrainers);

traineeRoutes.get("/info/trainer/:id", TraineeController.ShowTrainer);
traineeRoutes.get("/info/trainee/:id", TraineeController.ShowTrainee);

export default traineeRoutes;
