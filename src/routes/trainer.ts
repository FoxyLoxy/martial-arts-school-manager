import { Router } from 'express';
import * as SchoolController from "../controllers/trainer/school";
import * as BranchController from "../controllers/trainer/branch";
import * as TrainerController from "../controllers/trainer/trainer";
import * as TraineeController from "../controllers/trainer/trainee";
import * as ExamController from "../controllers/trainer/exam";
import * as SeminarController from "../controllers/trainer/seminar";
import * as BlogPostController from "../controllers/trainer/blogPost";
import { SchoolValidation } from "../validation/trainer/School";
import { BranchValidation } from "../validation/trainer/Branch";
import { TrainerValidation } from "../validation/trainer/Trainer";
import { TraineeValidation } from "../validation/trainer/Trainee";
import { ExamValidation } from "../validation/trainer/Exam";

const trainerRoutes = Router();

trainerRoutes.get("", TrainerController.Home);

trainerRoutes.get("/school", SchoolController.Index);
trainerRoutes.get("/school/edit/:id", SchoolController.Show);
trainerRoutes.get("/school/edit", SchoolController.Create);
trainerRoutes.post("/school/edit", SchoolValidation, SchoolController.Store);
trainerRoutes.post("/school/delete/:id", SchoolController.Delete);

trainerRoutes.get("/branch", BranchController.Index);
trainerRoutes.get("/branch/edit/:id", BranchController.Show);
trainerRoutes.get("/branch/edit", BranchController.Create);
trainerRoutes.post("/branch/edit", BranchValidation, BranchController.Store);
trainerRoutes.post("/branch/delete/:id", BranchController.Delete);

trainerRoutes.get("/trainer", TrainerController.Index);
trainerRoutes.get("/trainer/edit/:id", TrainerController.Show);
trainerRoutes.get("/trainer/edit", TrainerController.Create);
trainerRoutes.post("/trainer/edit", TrainerValidation, TrainerController.Store);
trainerRoutes.post("/trainer/delete/:id", TrainerController.Delete);

trainerRoutes.get("/trainee", TraineeController.Index);
trainerRoutes.get("/trainee/edit/:id", TraineeController.Show);
trainerRoutes.get("/trainee/edit", TraineeController.Create);
trainerRoutes.post("/trainee/edit", TraineeValidation, TraineeController.Store);
trainerRoutes.post("/trainee/delete/:id", TraineeController.Delete);

trainerRoutes.get("/exam", ExamController.Index);
trainerRoutes.get("/exam/edit/:id", ExamController.Show);
trainerRoutes.get("/exam/edit", ExamController.Create);
trainerRoutes.post("/exam/edit", ExamValidation, ExamController.Store);
trainerRoutes.post("/exam/delete/:id", ExamController.Delete);
trainerRoutes.get("/exam/show/:id", ExamController.ShowAllTraineeConclusion);
trainerRoutes.get("/exam/show/:examId/:traineeId", ExamController.ShowTraineeConclusion);
trainerRoutes.post("/exam/show/:examId/:traineeId", ExamController.StoreTraineeConclusion);

trainerRoutes.get("/seminar", SeminarController.Index);
trainerRoutes.get("/seminar/edit/:id", SeminarController.Show);
trainerRoutes.get("/seminar/edit", SeminarController.Create);
trainerRoutes.post("/seminar/edit", SeminarController.Store);
trainerRoutes.post("/seminar/delete/:id", SeminarController.Delete);
trainerRoutes.post("/seminar/protocol/:id", SeminarController.StoreProtocol);

trainerRoutes.get("/blogPost", BlogPostController.Index);
trainerRoutes.get("/blogPost/edit/:id", BlogPostController.Show);
trainerRoutes.get("/blogPost/edit", BlogPostController.Create);
trainerRoutes.post("/blogPost/edit", BlogPostController.Store);
trainerRoutes.post("/blogPost/delete/:id", BlogPostController.Delete);

export default trainerRoutes;
