import * as express from 'express';
import logger from 'morgan';
import * as bodyParser from "body-parser";
import helmet from "helmet";
import cors from "cors";
import session from "express-session";
import fileUpload from "express-fileupload";
import flash from "connect-flash";

import landingRoutes from "./routes/landing";
import traineeRoutes from "./routes/trainee";
import trainerRoutes from "./routes/trainer";

const app: express.Application = (express as any).default();

app.set('view engine', 'pug');

app.use(helmet());

app.use(cors());

app.use(session({
    secret: 'keyboard cat',
    cookie: {
        // secure: true,
        maxAge: 60000,
    },
}));

app.use(flash());

app.use(
    logger('dev', {
        skip: () => app.get('env') === 'test',
    })
);

app.use(bodyParser.urlencoded({
    extended: true,
}));

app.use(fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 },
}));

app.use('/public/files', express.static('public'));

app.use('/', landingRoutes);
app.use('/trainee', traineeRoutes);
app.use('/trainer', trainerRoutes);

export default app;
