import { check } from "express-validator";
import { School, SchoolDocument } from "../../models/school";
import { Branch, BranchDocument } from "../../models/branch";
import { Trainee } from "../../models/trainee";

export const ExamValidation = [
    check('date').custom(value => {
        if (isNaN(Date.parse(value))) {
            return Promise.reject("Invalid date");
        } else {
            return Promise.resolve();
        }
    }),
    check('branch_id').custom(value => {
        return Branch.findById(value).then((branch: BranchDocument | null) => {
            if (!branch) {
                return Promise.reject("Select a proper branch");
            } else {
                return Promise.resolve();
            }
        });
    }),
    check('selectedTrainees').isArray().withMessage("Trainees should be an array").custom(value => {
        let promises = [] as Promise<never>[];
        for (const id of value) {
            promises.push(Trainee.findById(id).catch(() => {
                return Promise.reject("All trainees should be valid");
            }));
        }

        return Promise.all(promises);
    })
];
