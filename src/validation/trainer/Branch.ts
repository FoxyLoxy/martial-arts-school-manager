import { check } from "express-validator";
import { School, SchoolDocument } from "../../models/school";

export const BranchValidation = [
    check('name').not().isEmpty().withMessage('Name must be not empty'),
    check('school_id').custom(value => {
        return School.findById(value).then((school: SchoolDocument | null) => {
            if (!school) {
                return Promise.reject("Select a proper school") as PromiseLike<never>;
            } else {
                return Promise.resolve();
            }
        });
    }),
];
