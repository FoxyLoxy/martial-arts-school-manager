import { check } from "express-validator";

export const SchoolValidation = [
    check('name').not().isEmpty().withMessage('Name must be not empty'),
    check('address').not().isEmpty().withMessage('Address must be not empty'),
];
