import { check, Meta } from "express-validator";
import { Branch, BranchDocument } from "../../models/branch";
import { levels } from "../../models/trainer";

export const TrainerValidation = [
    check('email').isEmail().withMessage('Should be valid email'),
    check('phone').isMobilePhone("uk-UA").withMessage('Should be valid phone'),
    check('password').if((val: any, meta: Meta) => {
        if (meta.req._id || meta.req._id !== "") {
            return Promise.resolve();
        } else {
            return Promise.reject("Password should be present during creation");
        }
    }),
    check('name').not().isEmpty().withMessage('Name must be not empty'),
    check('surname').not().isEmpty().withMessage('Surname must be not empty'),
    check('fathersName').not().isEmpty().withMessage('Fathers name must be not empty'),
    check('birthDate').custom(value => {
        if (isNaN(Date.parse(value))) {
            return Promise.reject("Invalid date");
        } else {
            return Promise.resolve();
        }
    }),
    check('level').isNumeric({
        no_symbols: true
    }).withMessage('Should be a valid number'),
    check('levelType').isIn(levels).withMessage('Should be or Que or Dan'),
    check('branch_id').custom(value => {
        return Branch.findById(value).then((branch: BranchDocument | null) => {
            if (!branch) {
                return Promise.reject("Select a proper branch");
            } else {
                return Promise.resolve();
            }
        });
    }),
];
