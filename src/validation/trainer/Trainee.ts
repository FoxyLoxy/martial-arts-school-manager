import { check, Meta } from "express-validator";
import { levels, Trainer, TrainerDocument } from "../../models/trainer";

export const TraineeValidation = [
    check('email').isEmail().withMessage('Should be valid email'),
    check('phone').isMobilePhone("uk-UA").withMessage('Should be valid phone'),
    check('name').not().isEmpty().withMessage('Name must be not empty'),
    check('surname').not().isEmpty().withMessage('Surname must be not empty'),
    check('fathersName').not().isEmpty().withMessage('Fathers name must be not empty'),
    check('birthDate').custom(value => {
        if (isNaN(Date.parse(value))) {
            return Promise.reject("Invalid date");
        } else {
            return Promise.resolve();
        }
    }),
    check('level').isNumeric({
        no_symbols: true
    }).withMessage('Should be a valid number'),
    check('levelType').isIn(levels).withMessage('Should be or Que or Dan'),
    check('trainer_id').custom(value => {
        return Trainer.findById(value).then((branch: TrainerDocument | null) => {
            if (!branch) {
                return Promise.reject("Select a proper trainer");
            } else {
                return Promise.resolve();
            }
        });
    }),
];
