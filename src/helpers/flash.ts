export interface Flash {
    type: "info" | "warning" | "success";
    message: string;
}
